import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GlobalVariables {
  static List<String> currentLocation = ["37.337515", "-121.881790"];
  static List<LatLng> waypointList = List<LatLng>.empty(growable: true);
}
