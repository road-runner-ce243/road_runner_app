import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:flutter/services.dart';
import 'package:rc_app/widgets/widgets.dart';
import 'package:rc_app/assets/GlobalVariables.dart';

class BLE extends StatefulWidget {
  BLE({Key? key, required this.title}) : super(key: key);

  final String title;
  final FlutterBlue flutterBlue = FlutterBlue.instance;
  final List<BluetoothDevice> devicesList = <BluetoothDevice>[];
  final Map<Guid, List<int>> readValues = <Guid, List<int>>{};

  @override
  State<BLE> createState() => _BLEState();
}

class _BLEState extends State<BLE> {
  final _writeController = TextEditingController();
  final ScrollController _controller = ScrollController();
  BluetoothDevice? _connectedDevice;
  late List<BluetoothService> _services;

  final String service_uuid = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
  final String rx_uuid = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
  final String tx_uuid = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";

  BluetoothCharacteristic? rx_characteristic;
  String rx_message = "Hi, press Read to start Reading";
  List<int> rx_data_assembled = [];

  List<String> currentLocation = ["", ""];
  List<String> destination = ["", ""];
  String leftSensor = "";
  String middleSensor = "";
  String rightSensor = "";
  String driverIntent = "";
  String driveDirection = "";
  String computedRpm = "";
  String compassHeading = "";
  String compassBearing = "";
  String distanceToDest = "";

  ClipboardData data = ClipboardData(text: "");

  _addDeviceTolist(final BluetoothDevice device) {
    if (!widget.devicesList.contains(device)) {
      setState(() {
        widget.devicesList.add(device);
      });
    }
  }

  @override
  void initState() {
    super.initState();
    widget.flutterBlue.connectedDevices
        .asStream()
        .listen((List<BluetoothDevice> devices) {
      for (BluetoothDevice device in devices) {
        _addDeviceTolist(device);
      }
    });
    widget.flutterBlue.scanResults.listen((List<ScanResult> results) {
      for (ScanResult result in results) {
        _addDeviceTolist(result.device);
      }
    });
    widget.flutterBlue.startScan();
  }

  ListView _buildListViewOfDevices() {
    List<Container> containers = <Container>[];
    for (BluetoothDevice device in widget.devicesList) {
      containers.add(
        Container(
          color: (device.name.compareTo("Adafruit Bluefruit LE") == 0
              ? Colors.lightGreen
              : null),
          height: 50,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  children: <Widget>[
                    Text(device.name == '' ? '(unknown device)' : device.name),
                    Text(device.id.toString()),
                  ],
                ),
              ),
              TextButton(
                child: const Text(
                  'Connect',
                  style: TextStyle(
                      color: Colors.black, backgroundColor: Colors.greenAccent),
                ),
                onPressed: () async {
                  widget.flutterBlue.stopScan();
                  try {
                    await device.connect();
                  } catch (e) {
                    print(e);
                  } finally {
                    _services = await device.discoverServices();
                  }
                  setState(() {
                    _connectedDevice = device;
                  });
                },
              ),
            ],
          ),
        ),
      );
    }

    return ListView(
      controller: _controller,
      shrinkWrap: true,
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }

  List<ButtonTheme> _buildReadWriteNotifyButton(
      BluetoothCharacteristic characteristic) {
    List<ButtonTheme> buttons = <ButtonTheme>[];

    if (characteristic.properties.read) {
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.greenAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('READ', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                var sub = characteristic.value.listen((value) {
                  setState(() {
                    widget.readValues[characteristic.uuid] = value;
                  });
                });
                await characteristic.read();
                sub.cancel();
              },
            ),
          ),
        ),
      );
    }
    if (characteristic.properties.write) {
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.greenAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('WRITE', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                data = (await Clipboard.getData('text/plain'))!;
                await showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text("Write"),
                        content: Row(
                          children: <Widget>[
                            Expanded(
                              child: Text(data.text!),
                            ),
                          ],
                        ),
                        actions: <Widget>[
                          TextButton(
                            child: const Text("Send"),
                            onPressed: () async {
                              characteristic.write(
                                  utf8.encode(data.text!.substring(0, 10)));
                              characteristic.write(utf8
                                  .encode(data.text!.substring(11, 23) + '\n'));
                              Navigator.pop(context);
                            },
                          ),
                          TextButton(
                            child: const Text("Cancel"),
                            onPressed: () {
                              Navigator.pop(context);
                            },
                          ),
                        ],
                      );
                    });
              },
            ),
          ),
        ),
      );
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.green,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('GO', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                characteristic.write(utf8.encode("!,GO"));
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const AlertDialog(
                        title: Text("Go!!!"),
                      );
                    });
              },
            ),
          ),
        ),
      );
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.redAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('STOP', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                characteristic.write(utf8.encode("!,STOP"));
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const AlertDialog(
                        title: Text("STOP!!!"),
                      );
                    });
              },
            ),
          ),
        ),
      );
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.blueAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('Send Waypoints',
                  style: TextStyle(color: Colors.black)),
              onPressed: () async {
                for (var waypoint in GlobalVariables.waypointList) {
                  String waypointString =
                      "W,${double.parse((waypoint.latitude).toStringAsFixed(6))},${double.parse((waypoint.longitude).toStringAsFixed(6))},";
                  characteristic
                      .write(utf8.encode(waypointString.substring(0, 10)));
                  characteristic.write(
                      utf8.encode(waypointString.substring(11, 23) + '\n'));
                }

                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const AlertDialog(
                        title: Text("Sent Waypoints"),
                      );
                    });
              },
            ),
          ),
        ),
      );
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.blueAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('Disconnect',
                  style: TextStyle(color: Colors.black)),
              onPressed: () async {
                setState(() {
                  _connectedDevice!.disconnect();
                  _connectedDevice = null;
                });
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const AlertDialog(
                        title: Text("Disconnected from car"),
                      );
                    });
              },
            ),
          ),
        ),
      );
    }
    if (characteristic.properties.notify) {
      buttons.add(
        ButtonTheme(
          minWidth: 10,
          height: 20,
          buttonColor: Colors.greenAccent,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
              child: const Text('READ', style: TextStyle(color: Colors.black)),
              onPressed: () async {
                rx_data_assembled.clear();
                var sub = characteristic.value.listen(
                  (value) {
                    setState(() {
                      if (value.first == 0x24) {
                        rx_data_assembled.clear();
                      }
                      rx_data_assembled.addAll(value);
                      widget.readValues[characteristic.uuid] = value;
                      rx_message = String.fromCharCodes(rx_data_assembled);
                      parseData();
                    });
                  },
                );
                await characteristic.setNotifyValue(true);
              },
            ),
          ),
        ),
      );
    }
    return buttons;
  }

  ListView _buildConnectDeviceView() {
    List<Container> containers = <Container>[];

    for (BluetoothService service in _services) {
      List<Widget> characteristicsWidget = <Widget>[];
      if (service.uuid.toString() == service_uuid) {
        for (BluetoothCharacteristic characteristic
            in service.characteristics) {
          if (characteristic.uuid.toString() == rx_uuid) {
            setState(() {
              rx_characteristic = characteristic;
            });
          }
          characteristicsWidget.add(
            Align(
              alignment: Alignment.centerLeft,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Wrap(
                    alignment: WrapAlignment.spaceBetween,
                    direction: Axis.horizontal,
                    children: <Widget>[
                      ..._buildReadWriteNotifyButton(characteristic),
                    ],
                  ),
                  const Divider(),
                ],
              ),
            ),
          );
        }

        containers.add(
          Container(
            child: ExpansionTile(
                title: Text("Commands"), children: characteristicsWidget),
          ),
        );
      }
    }

    return ListView(
      controller: _controller,
      shrinkWrap: true,
      padding: const EdgeInsets.all(8),
      children: <Widget>[
        ...containers,
      ],
    );
  }

  void parseData() {
    setState(() {
      String statusData = rx_message.substring(1);
      List<String> values = statusData.split(',');
      currentLocation[0] = values[0];
      currentLocation[1] = values[1];
      GlobalVariables.currentLocation[0] = values[0];
      GlobalVariables.currentLocation[1] = values[1];

      destination[0] = values[2];
      destination[1] = values[3];

      leftSensor = values[4];
      middleSensor = values[5];
      rightSensor = values[6];

      computedRpm = values[7];
      compassHeading = values[8];
      compassBearing = values[9];
      distanceToDest = values[10];
    });
  }

  ListView _buildView() {
    if (_connectedDevice != null) {
      return _buildConnectDeviceView();
    }
    return _buildListViewOfDevices();
  }

  @override
  Widget build(BuildContext context) => Container(
          child: Column(
        children: [
          _buildView(),
          const Text("Telemetry"),
          StatusTable(
            currentLocation: currentLocation,
            destination: destination,
            leftSensor: leftSensor,
            middleSensor: middleSensor,
            rightSensor: rightSensor,
            computedRpm: computedRpm,
            compassHeading: compassHeading,
            compassBearing: compassBearing,
            distanceToDest: distanceToDest,
          )
        ],
      ));
}
