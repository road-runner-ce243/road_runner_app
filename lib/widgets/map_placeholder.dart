import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rc_app/src/locations.dart' as locations;
import 'package:flutter/services.dart';
import 'package:rc_app/assets/GlobalVariables.dart';

class MapWidget extends StatefulWidget {
  const MapWidget({Key? key}) : super(key: key);

  @override
  State<MapWidget> createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  final Map<String, Marker> _markers = {};
  final LatLng _center = const LatLng(37.337515, -121.881790);
  late String _now;
  late Timer _everyHalfSecond;

  @override
  void initState() {
    super.initState();
    _now = DateTime.now().millisecond.toString();

    _everyHalfSecond =
        Timer.periodic(const Duration(milliseconds: 500), (Timer t) {
      setState(() {
        LatLng car_location = LatLng(
            double.parse(GlobalVariables.currentLocation[0]),
            double.parse(GlobalVariables.currentLocation[1]));
        _now = DateTime.now().millisecond.toString();
        final MarkerId markerId = MarkerId("RoadRunner");
        Marker marker = Marker(
          markerId: markerId,
          draggable: true,
          position: car_location,
          infoWindow: InfoWindow(
              title: "RoadRunner",
              snippet: car_location.toJson().toString(),
              onTap: () {}),
          icon:
              BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        );
        _markers["RoadRunner"] = marker;
      });
    });
  }

  Future<void> _onMapCreated(GoogleMapController controller) async {
    final googleOffices = await locations.getGoogleOffices();
    setState(() {
      _markers.clear();
      GlobalVariables.waypointList.clear();
    });
  }

  Future _copyCoordinates(LatLng latlang) async {
    setState(() {
      String destString =
          "\$,${double.parse((latlang.latitude).toStringAsFixed(6))},${double.parse((latlang.longitude).toStringAsFixed(6))},";

      Clipboard.setData(ClipboardData(text: destString));
    });
  }

  Future _addMarkerLongPressed(LatLng latlang) async {
    setState(() {
      _copyCoordinates(latlang);
      final MarkerId markerId = MarkerId("Destination");
      Marker marker = Marker(
        markerId: markerId,
        draggable: true,
        position:
            latlang, //With this parameter you automatically obtain latitude and longitude
        infoWindow: InfoWindow(
            title: "Destination",
            snippet: latlang.toJson().toString(),
            onTap: () {
              _markers.clear();
              GlobalVariables.waypointList.clear();
            }),
        icon: BitmapDescriptor.defaultMarker,
      );

      _markers["dest"] = marker;
    });
  }

  Future _addWaypointOnTap(LatLng latlang) async {
    setState(() {
      GlobalVariables.waypointList.add(latlang);
      final MarkerId markerId =
          MarkerId(GlobalVariables.waypointList.length.toString());
      Marker marker = Marker(
        markerId: markerId,
        draggable: true,
        position:
            latlang, //With this parameter you automatically obtain latitude and longitude
        infoWindow: InfoWindow(
            title: "Waypoint " + GlobalVariables.waypointList.length.toString(),
            snippet: latlang.toJson().toString(),
            onTap: () {
              // _copyCoordinates(latlang);
            }),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange),
      );
      _markers[GlobalVariables.waypointList.length.toString()] = marker;
    });
  }

  Future _removeMarkerOnTap(LatLng latlang) async {
    setState(() {
      _markers.clear();
      GlobalVariables.waypointList.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(8),
      height: 300,
      decoration: const BoxDecoration(
          color: Colors.blueGrey, shape: BoxShape.rectangle),
      child: GoogleMap(
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>{
          Factory<OneSequenceGestureRecognizer>(
            () => EagerGestureRecognizer(),
          )
        },
        onMapCreated: _onMapCreated,
        initialCameraPosition: CameraPosition(
          target: _center,
          zoom: 15,
        ),
        onTap: (latlang) {
          _addWaypointOnTap(latlang);
        },
        onLongPress: (latlang) {
          _addMarkerLongPressed(latlang);
        },
        markers: _markers.values.toSet(),
      ),
    );
  }
}
