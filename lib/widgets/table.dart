import 'package:flutter/material.dart';

class StatusTable extends StatefulWidget {
  final List<String> currentLocation;
  final List<String> destination;
  final String leftSensor;
  final String middleSensor;
  final String rightSensor;
  final String computedRpm;
  final String compassHeading;
  final String compassBearing;
  final String distanceToDest;

  const StatusTable(
      {Key? key,
      required this.currentLocation,
      required this.destination,
      required this.leftSensor,
      required this.middleSensor,
      required this.rightSensor,
      required this.computedRpm,
      required this.compassHeading,
      required this.compassBearing,
      required this.distanceToDest})
      : super(key: key);

  @override
  State<StatusTable> createState() => _StatusTableState();
}

class _StatusTableState extends State<StatusTable> {
  @override
  Widget build(BuildContext context) {
    return Table(
      border: TableBorder.all(),
      columnWidths: const <int, TableColumnWidth>{
        0: IntrinsicColumnWidth(),
        1: FlexColumnWidth(),
        2: FixedColumnWidth(64),
      },
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: <TableRow>[
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              width: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Current Location",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 32,
                color: Colors.blueAccent,
                child: Center(
                  child: Text(
                    "${widget.currentLocation[0]}, ${widget.currentLocation[1]}",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Destination",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 32,
                color: Colors.greenAccent,
                child: Center(
                  child: Text(
                    "${widget.destination[0]}, ${widget.destination[1]}",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.greenAccent,
              child: const Center(
                child: Text(
                  "Left Sensor",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 32,
                color: (widget.leftSensor != "" &&
                        int.parse(widget.leftSensor) > 120)
                    ? Colors.greenAccent
                    : Colors.redAccent,
                child: Center(
                  child: Text(
                    widget.leftSensor,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Middle Sensor",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 32,
                color: (widget.middleSensor != "" &&
                        int.parse(widget.middleSensor) > 120)
                    ? Colors.greenAccent
                    : Colors.redAccent,
                child: Center(
                  child: Text(
                    widget.middleSensor,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Right Sensor",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 32,
                color: (widget.rightSensor != "" &&
                        int.parse(widget.rightSensor) > 120)
                    ? Colors.greenAccent
                    : Colors.redAccent,
                child: Center(
                  child: Text(
                    widget.rightSensor,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "RPM",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 32,
                color: Colors.green,
                child: Center(
                  child: Text(
                    widget.computedRpm,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Compass Heading",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 64,
                color: Colors.green,
                child: Center(
                  child: Text(
                    widget.compassHeading + "\u00B0",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Compass Bearing",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 64,
                color: (widget.compassBearing != "" &&
                        int.parse(widget.compassBearing) ==
                            int.parse(widget.compassHeading))
                    ? Colors.greenAccent
                    : Colors.yellowAccent,
                child: Center(
                  child: Text(
                    widget.compassBearing + "\u00B0",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            Container(
              height: 64,
              // color: Colors.green,
              child: const Center(
                child: Text(
                  "Destination Distance",
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            TableCell(
              verticalAlignment: TableCellVerticalAlignment.middle,
              child: Container(
                height: 64,
                width: 64,
                color: Colors.green,
                child: Center(
                  child: Text(
                    widget.distanceToDest + "m",
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
